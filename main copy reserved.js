import './style.css';
import * as THREE from 'three';

const scene = new THREE.Scene();
// scene == container


const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1 , 1000 );
// 1 - field of view, 2 - aspect ratio based on user's browser window, 3 - view frustrum to control which objects are visible to camera itself

const renderer = new THREE.WebGL1Renderer({
  canvas: document.querySelector('#bg'),
  
});

renderer.setPixelRatio( window.devicePixelRatio);
renderer.setSize ( window.innerWidth, window.innerHeight); //fullscreen
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;

camera.position.setZ(30);
camera.position.setX(-3);

renderer.render (scene, camera);

const spaceTexture = new THREE.TextureLoader().load("space.jpg");
scene.background = spaceTexture;

// Torus ring
const geometry = new THREE.TorusGeometry(10, 3, 16, 100);
const material = new THREE.MeshStandardMaterial ( { color: 0xFF6347 } );
  //MeshBasicMaterial requires no light source, MeshStandartMaterial reacts to lightning
  const torus = new THREE.Mesh( geometry, material);

  
  scene.add(torus)

  // Lights
  const pointLight = new THREE.PointLight(0xffffff);//aka lightbulb
  pointLight.position.set(5, 5, 5);
  
  pointLight.castShadow = true;


  const ambientLight = new THREE.AmbientLight(0xffffff);
  scene.add(pointLight, ambientLight);

 


  // const lightHelper = new THREE.PointLightHelper(pointLight);
  // scene.add(lightHelper); показывает источник света на сцене

//Portal cube
const cubeTexture = new THREE.TextureLoader().load("cube.png");
const cube = new THREE.Mesh(
  new THREE.BoxGeometry( 3, 3, 3,),
  new THREE.MeshBasicMaterial({ map: cubeTexture })
  
);

cube.position.z = -5;
cube.position.x = 2;

scene.add(cube);

//Avatar 
const avatarTexture = new THREE.TextureLoader().load('me.png');


const avatar = new THREE.Mesh(
  new THREE.SphereGeometry( 2, 5, 8),
  new THREE.MeshStandardMaterial( {
    map: avatarTexture
  })
);

avatar.position.z = 0.5;
avatar.position.y = 0.5;
scene.add(avatar);



//Moon sphere
const moonTexture = new THREE.TextureLoader().load('moon.jpg');
const normalTexture = new THREE.TextureLoader().load('normal.jpg');

const moon = new THREE.Mesh(
  new THREE.SphereGeometry( 3, 32, 32),
  new THREE.MeshStandardMaterial( {
    map: moonTexture,
    normalMap: normalTexture,
  })
);
moon.position.z = 30;
moon.position.setX(-10);

scene.add(moon);

function addStar() {
  const geometry = new THREE.SphereGeometry(0.25, 24, 24);
  const material = new THREE.MeshStandardMaterial ( { color: 0xFFFFFF } );
  const star = new THREE.Mesh(geometry, material);
  const [x, y, z] = Array(3)
  .fill()
  .map(() => THREE.MathUtils.randFloatSpread(100));

star.position.set(x, y, z);
scene.add(star);
}

Array(200).fill().forEach(addStar);

// Scroll Animation

function moveCamera() {
  const t = document.body.getBoundingClientRect().top;


  cube.rotation.y += 0.01;
  cube.rotation.z += 0.01;

  
 

  camera.position.z = t * -0.01;
  camera.position.x = t * -0.0002;
  camera.rotation.y = t * -0.0002;
}

document.body.onscroll = moveCamera
moveCamera();

  function animate (){
    requestAnimationFrame (animate);

    torus.rotation.x += 0.01; // top value always negative
    torus.rotation.y += 0.005;
    torus.rotation.z += 0.01;


      moon.rotation.y += 0.00099;

      avatar.translateX( 0.05 );
      avatar.rotation.y += 0.005;
    
    
    renderer.render (scene, camera);  
  }

  animate ()